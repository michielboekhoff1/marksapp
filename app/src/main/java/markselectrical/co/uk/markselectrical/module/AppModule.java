package markselectrical.co.uk.markselectrical.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import markselectrical.co.uk.markselectrical.App;
import markselectrical.co.uk.markselectrical.auth.AuthenticationService;
import markselectrical.co.uk.markselectrical.auth.CognitoAuthenticationService;

@Module
public class AppModule {
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    AuthenticationService providesAuthenticationService() {
        return new CognitoAuthenticationService(providesBaseContext());
    }

    @Provides
    @Singleton
    Context providesBaseContext() {
        return application.getApplicationContext();
    }
}
