package markselectrical.co.uk.markselectrical.auth;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

abstract class AsyncAuthenticationService implements AuthenticationService {
    protected ExecutorService getExecutorService() {
        return Executors.newSingleThreadExecutor();
    }
}
