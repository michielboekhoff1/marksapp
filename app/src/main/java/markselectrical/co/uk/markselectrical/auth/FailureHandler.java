package markselectrical.co.uk.markselectrical.auth;


public interface FailureHandler {
    void onFailure();
}
