package markselectrical.co.uk.markselectrical.auth;


public final class NotAuthorizedException extends Exception {
    public NotAuthorizedException(String username) {
        super(String.format("Username/password combo for user %s is incorrect", username));
    }
}
