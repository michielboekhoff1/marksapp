package markselectrical.co.uk.markselectrical.auth;


import javax.annotation.Nonnull;
import javax.annotation.Nullable;

class AuthResponse<T> {
    private Status status;
    private String message;
    private T response;

    public enum Status {
        AUTH_OK,
        AUTH_ERR
    };

    public AuthResponse(@Nonnull Status status, @Nullable String message) {
        this.status = status;
        this.message = message;
    }

    public AuthResponse(@Nonnull T response) {
        this.status = Status.AUTH_OK;
        this.response = response;
    }

    public AuthResponse() {
        this.status = Status.AUTH_ERR;
    }

    public boolean ok() {
        return status.equals(Status.AUTH_OK);
    }

    @Nullable
    public T getResponse() {
        return response;
    }

    public void setResponse(@Nonnull T response) {
        this.response = response;
    }

    @Nonnull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@Nonnull Status status) {

    }
}
