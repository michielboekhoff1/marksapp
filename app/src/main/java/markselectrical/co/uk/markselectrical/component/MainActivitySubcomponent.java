package markselectrical.co.uk.markselectrical.component;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import markselectrical.co.uk.markselectrical.MainActivity;

@Subcomponent
public interface MainActivitySubcomponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<MainActivity> {}
}
