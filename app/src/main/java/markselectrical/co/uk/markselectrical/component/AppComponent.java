package markselectrical.co.uk.markselectrical.component;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import markselectrical.co.uk.markselectrical.App;
import markselectrical.co.uk.markselectrical.module.AppModule;
import markselectrical.co.uk.markselectrical.module.LoginActivityModule;
import markselectrical.co.uk.markselectrical.module.MainActivityModule;

@Singleton
@Component(modules =  {
        AndroidInjectionModule.class, AppModule.class,
        MainActivityModule.class, LoginActivityModule.class
})
public interface AppComponent {
    void inject(App application);
}
