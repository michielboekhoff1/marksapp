package markselectrical.co.uk.markselectrical.module;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import markselectrical.co.uk.markselectrical.MainActivity;
import markselectrical.co.uk.markselectrical.component.MainActivitySubcomponent;

@Module(subcomponents = MainActivitySubcomponent.class)
public abstract class MainActivityModule {
    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
        bindYourActivityInjectorFactory(MainActivitySubcomponent.Builder builder);
}
