package markselectrical.co.uk.markselectrical.module;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import markselectrical.co.uk.markselectrical.component.LoginActivitySubcomponent;
import markselectrical.co.uk.markselectrical.login.LoginActivity;

@Module(subcomponents = LoginActivitySubcomponent.class)
public abstract class LoginActivityModule {
    @Binds
    @IntoMap
    @ActivityKey(LoginActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindYourActivityInjectorFactory(LoginActivitySubcomponent.Builder builder);
}
