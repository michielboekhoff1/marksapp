package markselectrical.co.uk.markselectrical.auth;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface AuthenticationService {
    @Nonnull User authenticateUser(AuthDetails authenticationDetails)
            throws NotAuthorizedException, TimeoutException;

    @Nullable User getCurrentUser();

    void signOut();
}
