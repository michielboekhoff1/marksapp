package markselectrical.co.uk.markselectrical.auth;


public final class TimeoutException extends Exception {
    public TimeoutException() {
        super("Authentication timed out.");
    }
}
