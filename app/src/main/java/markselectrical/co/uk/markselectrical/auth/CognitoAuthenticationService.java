package markselectrical.co.uk.markselectrical.auth;


import android.content.Context;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.regions.Regions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

public final class CognitoAuthenticationService extends AsyncAuthenticationService {

    private final CognitoUserPool userPool;

    @Inject
    public CognitoAuthenticationService(Context context) {
        userPool = new CognitoUserPool(
            context,
            "eu-west-1_YC4H22o02",
            "5blnjl804c70c4qrne3t2a9410",
            "1lnpo5m8vemngoboccol6n68pi11ba5stiptldhgcuq9keoiq6uj",
            Regions.EU_WEST_1
        );
    }

    @Override
    @Nonnull
    public User authenticateUser(final AuthDetails authenticationDetails)
            throws NotAuthorizedException, TimeoutException {
        final AuthenticationDetails authDetails = toDomainObject(authenticationDetails);

        final AuthResponse<User> response = new AuthResponse<>();

        synchronized (response) {
            try {
                userPool.getUser(authenticationDetails.getUsername()).getSessionInBackground(new AuthenticationHandler() {
                    @Override
                    public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
                        User user = sessionToUser(authenticationDetails.getUsername(), userSession);
                        response.setResponse(user);
                        response.setStatus(AuthResponse.Status.AUTH_OK);
                        response.notify();
                    }

                    @Override
                    public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
                        authenticationContinuation.setAuthenticationDetails(authDetails);
                        authenticationContinuation.continueTask();
                    }

                    @Override
                    public void getMFACode(MultiFactorAuthenticationContinuation continuation) {
                        response.setStatus(AuthResponse.Status.AUTH_ERR);
                        response.notify();
                    }

                    @Override
                    public void authenticationChallenge(ChallengeContinuation continuation) {
                        response.setStatus(AuthResponse.Status.AUTH_ERR);
                        response.notify();
                    }

                    @Override
                    public void onFailure(Exception exception) {
                        response.setStatus(AuthResponse.Status.AUTH_ERR);
                        response.notify();
                    }
                });

                response.wait(10 * 1000);
            } catch(InterruptedException e) {
                throw new TimeoutException();
            }
        }

        if (!response.getStatus().equals(AuthResponse.Status.AUTH_OK) ||
                response.getResponse() == null) {
            throw new NotAuthorizedException(authenticationDetails.getUsername());
        }

        return response.getResponse();
    }

    @Override
    @Nullable
    public User getCurrentUser() {

        final AuthResponse<User> response = new AuthResponse<>();

        synchronized (response) {
            try {
                final CognitoUser cognitoUser = userPool.getCurrentUser();
                cognitoUser.getSession(new AuthenticationHandler() {
                    @Override
                    public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
                        User user = sessionToUser(cognitoUser.getUserId(), userSession);
                        response.setResponse(user);
                        response.notify();
                    }

                    @Override
                    public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
                        response.notify();
                    }

                    @Override
                    public void getMFACode(MultiFactorAuthenticationContinuation continuation) {
                        response.notify();
                    }

                    @Override
                    public void authenticationChallenge(ChallengeContinuation continuation) {
                        response.notify();
                    }

                    @Override
                    public void onFailure(Exception exception) {
                        response.notify();
                    }
                });

                response.wait(10 * 1000);
            } catch (InterruptedException e) {
                return null;
            }
        }

        if (!response.getStatus().equals(AuthResponse.Status.AUTH_OK)) {
            return null;
        }


        return response.getResponse();
    }

    @Override
    public void signOut() {
        userPool.getCurrentUser().signOut();
    }

    private User sessionToUser(@Nonnull String username, CognitoUserSession session) {
        return new User(
                username,
                session.getAccessToken().getJWTToken(),
                session.getIdToken().getJWTToken()
        );
    }

    private AuthenticationDetails toDomainObject(AuthDetails authenticationDetails) {
        return new AuthenticationDetails(
                authenticationDetails.getUsername(),
                authenticationDetails.getPassword(),
                null
        );
    }
}
