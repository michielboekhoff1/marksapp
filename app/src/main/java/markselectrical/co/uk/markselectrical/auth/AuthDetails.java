package markselectrical.co.uk.markselectrical.auth;


import javax.annotation.Nonnull;

public class AuthDetails {
    private final String username;
    private final String password;

    public AuthDetails(@Nonnull String username, @Nonnull String password) {
        this.username = username;
        this.password = password;
    }

    @Nonnull
    public String getUsername() {
        return username;
    }

    @Nonnull
    public String getPassword() {
        return password;
    }
}
