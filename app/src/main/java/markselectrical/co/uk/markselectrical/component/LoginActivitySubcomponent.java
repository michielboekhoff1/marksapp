package markselectrical.co.uk.markselectrical.component;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import markselectrical.co.uk.markselectrical.login.LoginActivity;

@Subcomponent
public interface LoginActivitySubcomponent extends AndroidInjector<LoginActivity> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<LoginActivity> {}
}
