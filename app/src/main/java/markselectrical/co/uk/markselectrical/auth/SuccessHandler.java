package markselectrical.co.uk.markselectrical.auth;


public interface SuccessHandler {
    void onSuccess(User user);
}
