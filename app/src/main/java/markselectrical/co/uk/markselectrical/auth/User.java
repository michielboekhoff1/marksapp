package markselectrical.co.uk.markselectrical.auth;

import android.support.annotation.NonNull;

import java.util.Objects;

import javax.annotation.Nonnull;


public final class User {
    @NonNull
    private final String name;

    @NonNull
    private final String accessToken;

    @Nonnull
    private final String idToken;

    public User(@NonNull String name, @NonNull String accessToken, @Nonnull String idToken) {
        this.name = name;
        this.accessToken = accessToken;
        this.idToken = idToken;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Nonnull
    String getAccessToken() {
        return accessToken;
    }

    @Nonnull
    String getIdToken() {
        return idToken;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, accessToken, idToken);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        if (hashCode() != obj.hashCode()) return false;

        User userObj = (User) obj;

        return Objects.equals(name, userObj.name) &&
                Objects.equals(accessToken, userObj.accessToken) &&
                Objects.equals(idToken, userObj.idToken);
    }
}
