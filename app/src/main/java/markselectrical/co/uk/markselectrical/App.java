package markselectrical.co.uk.markselectrical;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import markselectrical.co.uk.markselectrical.component.AppComponent;
//import markselectrical.co.uk.markselectrical.component.DaggerAppComponent;
//import markselectrical.co.uk.markselectrical.module.AppModule;

public class App extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = createComponent();
        appComponent.inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    private AppComponent createComponent() {
        //return DaggerAppComponent.builder()
                //.appModule(new AppModule(this))
                //.build();
        return null;
    }
}
